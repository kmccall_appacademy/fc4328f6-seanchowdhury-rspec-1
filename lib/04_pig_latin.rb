def translate(str)
  str.split(" ").map {|i| translate_word(i)}.join(" ")
end

def translate_word(str)
  arr = str.chars
  until arr[0] =~ /[aeiou]/
    arr.rotate! if arr[0] == "q"
    arr.rotate!
  end
  arr.join + "ay"
end
