def add(num1,num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(arr)
  return 0 if arr.empty?
  arr.inject(:+)
end

def multiply(arr)
  arr.inject(:*)
end

def power(num1,num2)
  num1 ** num2
end

def factorial(num)
  (1..num).to_a.inject(:*)
end
