def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str,num = 2)
  arr = []
  num.times {arr << str}
  arr.join(" ")
end

def start_of_word(str,num)
  str[0...num]
end

def first_word(str)
  str.split(" ").first
end

def titleize(str)
  arr = str.split(" ").map do |i|
    if i == "and" || i == "or" || i == "over" || i == "the"
      i
    else
      i.capitalize
    end
  end
  arr[0].capitalize!
  arr.join(" ")
end
